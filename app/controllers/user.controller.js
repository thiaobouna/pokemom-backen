const user = require("../models/user.model.js");


//creation objet
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    // Create a user
    const users = new user({
      username: req.body.username,
      password: req.body.password,
      etat: req.body.etat,
    });
  
    // Save user in the database
    user.create(users, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the pokemon."
        });
      else res.send(data);
    });
  };

  
  // Retrieve all pokemon from the database (with condition).
exports.findAll = (req, res) => {
    const username = req.query.username;
  
    user.getAll(username, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving pokemon."
        });
      else res.send(data);
    });
  };
  
  //recuperer un user
  exports.findOne = (req, res) => {
    user.findById(req.params.id, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found user with id ${req.params.id}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving user with id " + req.params.id
          });
        }
      } else res.send(data);
    });
  };

  //modifier user
  exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    console.log(req.body);
  
    user.updateById(
      req.params.id,
      new user(req.body),
      (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found user with id ${req.params.id}.`
            });
          } else {
            res.status(500).send({
              message: "Error updating user with id " + req.params.id
            });
          }
        } else res.send(data);
      }
    );
  };

  //supprimer user
  exports.delete = (req, res) => {
    user.remove(req.params.id, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found user with id ${req.params.id}.`
          });
        } else {
          res.status(500).send({
            message: "Could not delete user with id " + req.params.id
          });
        }
      } else res.send({ message: `user was deleted successfully!` });
    });
  };


  //supprimer tout
  exports.deleteAll = (req, res) => {
    user.removeAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all user."
        });
      else res.send({ message: `All user were deleted successfully!` });
    });
  };

 
