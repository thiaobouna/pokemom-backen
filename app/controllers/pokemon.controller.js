const pokemon = require("../models/pokemon.model.js");
//creation objet
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    // Create a Tutorial
    const pokemons = new pokemon({
      name: req.body.name,
      hp: req.body.hp,
      cp: req.body.cp,
      picture: req.body.picture,
      types: req.body.types,
      created: req.body.created,
    });
  
    // Save pokemon in the database
    pokemon.create(pokemons, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the pokemon."
        });
      else res.send(data);
    });
  };

  
  // Retrieve all pokemon from the database (with condition).
exports.findAll = (req, res) => {
    const name = req.query.name;
  
    pokemon.getAll(name, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving pokemon."
        });
      else res.send(data);
    });
  };
  
  //recuperer un pokemon
  exports.findOne = (req, res) => {
    pokemon.findById(req.params.id, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found pokemon with id ${req.params.id}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving Tutorial with id " + req.params.id
          });
        }
      } else res.send(data);
    });
  };

  //modifier pokemon
  exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    console.log(req.body);
  
    pokemon.updateById(
      req.params.id,
      new pokemon(req.body),
      (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found pokemon with id ${req.params.id}.`
            });
          } else {
            res.status(500).send({
              message: "Error updating pokemon with id " + req.params.id
            });
          }
        } else res.send(data);
      }
    );
  };

  //supprimer pokemon
  exports.delete = (req, res) => {
    pokemon.remove(req.params.id, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found pokemon with id ${req.params.id}.`
          });
        } else {
          res.status(500).send({
            message: "Could not delete pokemon with id " + req.params.id
          });
        }
      } else res.send({ message: `pokemon was deleted successfully!` });
    });
  };


  //supprimer tout
  exports.deleteAll = (req, res) => {
    pokemon.removeAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all pokemon."
        });
      else res.send({ message: `All pokemon were deleted successfully!` });
    });
  };

  