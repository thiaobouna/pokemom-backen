module.exports = app => {
    const pokemon = require("../controllers/pokemon.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", pokemon.create);
  
    // Retrieve all Tutorials
    router.get("/", pokemon.findAll);
  
    // Retrieve a single Tutorial with id
    router.get("/:id", pokemon.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", pokemon.update);
  
    // Delete a Tutorial with id
    router.delete("/:id", pokemon.delete);
  
    // Delete all Tutorials
    router.delete("/", pokemon.deleteAll);
  
    app.use('/api/pokemon', router);
  };

