const sql = require("./db.js");
  
  
// constructor pokemon
const pokemon = function(pokemon) {
  this.name = pokemon.name;
  this.hp = pokemon.hp;
  this.cp = pokemon.cp;
  this.picture = pokemon.picture;
  this.types = pokemon.types;
  this.created = pokemon.created;
};

pokemon.create = (newpokemon, result) => {
  sql.query("INSERT INTO pokemon SET ?", newpokemon, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created pokemon: ", { id: res.insertId, ...newpokemon });
    result(null, { id: res.insertId, ...newpokemon });
  });
};


//truver un pokemon
pokemon.findById = (id, result) => {
    sql.query(`SELECT * FROM pokemon WHERE id = ${id}`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("found pokemon: ", res[0]);
        result(null, res[0]);
        return;
      }
  
      // not found pokemon with the id
      result({ kind: "not_found" }, null);
    });
  };

  //afficher tout les pokemon
  pokemon.getAll = (name, result) => {
    let query = "SELECT * FROM pokemon";
  
    if (name) {
      query += ` WHERE name LIKE '%${name}%'`;
    }
  
    sql.query(query, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("pokemon: ", res);
      result(null, res);
    });
  };

  //modifier pokemon
  pokemon.updateById = (id, pokemon, result) => {
    sql.query(
      "UPDATE pokemon SET name = ?, hp = ?, cp = ?, picture = ?, types = ?,created = ? WHERE id = ?",
      [pokemon.name, pokemon.hp, pokemon.cp,pokemon.picture,pokemon.types,pokemon.created, id],
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
  
        if (res.affectedRows == 0) {
          // not found Tutorial with the id
          result({ kind: "not_found" }, null);
          return;
        }
  
        console.log("updated pokemon: ", { id: id, ...pokemon });
        result(null, { id: id, ...pokemon });
      }
    );
  };

  //supprimer un pokemon
  pokemon.remove = (id, result) => {
    sql.query("DELETE FROM pokemon WHERE id = ?", id, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      if (res.affectedRows == 0) {
        // not found pokemon with the id
        result({ kind: "not_found" }, null);
        return;
      }
  
      console.log("deleted pokemon with id: ", id);
      result(null, res);
    });
  };

  //supprimer tout
  pokemon.removeAll = result => {
    sql.query("DELETE FROM pokemon", (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log(`deleted ${res.affectedRows} pokemon`);
      result(null, res);
    });
  };
  
  module.exports = pokemon;
  