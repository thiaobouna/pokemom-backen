const sql = require("./db.js");
  
  
// constructor pokemon
const user = function(user) {
  this.username = user.username;
  this.password = user.password;
  this.etat = user.etat;
};

user.create = (newuser, result) => {
  sql.query("INSERT INTO user SET ?", newuser, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created user: ", { id: res.insertId, ...newuser });
    result(null, { id: res.insertId, ...newuser });
  });
};


//truver un usre
user.findById = (id, result) => {
    sql.query(`SELECT * FROM user WHERE id = ${id}`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("found user: ", res[0]);
        result(null, res[0]);
        return;
      }
  
      // not found pokemon with the id
      result({ kind: "not_found" }, null);
    });
  };

  //afficher tout les user
  user.getAll = (username, result) => {
    let query = "SELECT * FROM user";
  
    if (username) {
      query += ` WHERE username LIKE '%${username}%'`;
    }
  
    sql.query(query, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("user: ", res);
      result(null, res);
    });
  };

  //modifier user
  user.updateById = (id, user, result) => {
    sql.query(
      "UPDATE user SET username = ?, password = ?, etat = ? WHERE id = ?",
      [user.username, user.password, user.etat, id],
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
  
        if (res.affectedRows == 0) {
          // not found Tutorial with the id
          result({ kind: "not_found" }, null);
          return;
        }
  
        console.log("updated user: ", { id: id, ...user });
        result(null, { id: id, ...user });
      }
    );
  };

  //supprimer un user
  user.remove = (id, result) => {
    sql.query("DELETE FROM user WHERE id = ?", id, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      if (res.affectedRows == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }
  
      console.log("deleted user with id: ", id);
      result(null, res);
    });
  };

  //supprimer tout
  user.removeAll = result => {
    sql.query("DELETE FROM user", (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log(`deleted ${res.affectedRows} user`);
      result(null, res);
    });
  };
  
  module.exports = user;